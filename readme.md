# stowed dotfiles
This repository contains packages of stowed files. They are "my precious" ;-). These stows are relative to $HOME and can be linked from the home directory using GNU stow.

# usage
Clone the repository into $HOME and enter the cloned directory. Use the command: "stow package" to create the links to your stow directory for the specific package.

## examples
- stow bash                    #used  to create links to the bash package
- stow -D bash                 #delete links for the bash package
- stow -R bash                 #recreate links for the bash package
