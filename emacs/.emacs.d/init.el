;;; CUSTOM VARIABLES AND CUSTOM FACES SECTION AUTOMATICALLY GENERATED
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("d14f3df28603e9517eb8fb7518b662d653b25b26e83bd8e129acea042b774298" "7661b762556018a44a29477b84757994d8386d6edee909409fabe0631952dad9" "4eb6fa2ee436e943b168a0cd8eab11afc0752aebb5d974bba2b2ddc8910fca8f" "83e0376b5df8d6a3fbdfffb9fb0e8cf41a11799d9471293a810deb7586c131e6" "6b5c518d1c250a8ce17463b7e435e9e20faa84f3f7defba8b579d4f5925f60c1" "78c4238956c3000f977300c8a079a3a8a8d4d9fee2e68bad91123b58a4aa8588" default))
 '(package-selected-packages
   '(org-roam consult orderless marginalia marginalla gruvbox-theme evil-collection undo-fu use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;;; ====================================================


;;; Startup
(set-face-attribute 'default nil :font "VictorMono Nerd Font Mono" :height 165)
(global-display-line-numbers-mode)             ;; line numbering
(setq display-line-numbers-type 'relative)     ;; relative line numbers
(fset 'yes-or-no-p 'y-or-n-p)                  ;; only y/n/p
(setenv "HOME" "/home/grazzhopper/")           ;; setting Home
(global-hl-line-mode t)                        ;; activate current line
(scroll-bar-mode -1)
(tool-bar-mode -1)
(menu-bar-mode -1)
(setq inhibit-startup-message t)
(setq vc-follow-symlinks t)

;;; LIST OF PACKAGES
(setq package-archives
      '(("melpa" . "https://melpa.org/packages/")
	("org" . "https://orgmode.org/elpa/")
	("elpa" . "https://elpa.gnu.org/packages/")))

;;; BOOTSTRAP USE-PACKAGE
(package-initialize)
(setq use-package-always-ensure t)
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-when-compile (require 'use-package))

;;; Gruvbox theming
(use-package gruvbox-theme
  :init
  (load-theme 'gruvbox-dark-hard))


;;; UNDO - Vim style
(use-package undo-fu)

;;; Vim bindings
(use-package evil
  :demand t
  :bind (("<escape>" . keyboard-escape-quit))
  :init
  ;; allows for using cgn
  ;; (setq evil-search-module 'evil-search)
  (setq evil-want-keybinding nil)
  ;; no vim insert bindings
  (setq evil-undo-system 'undo-fu)
  :config
  (evil-mode 1))

;;; Vim bindings everywhere else
(use-package evil-collection
  :after evil
  :config
  (setq evil-want-integration t)
  (evil-collection-init))

;;; Vim powerline
(use-package powerline-evil
  :ensure t)
(powerline-evil-vim-theme)
(powerline-evil-vim-color-theme)
(define-key evil-ex-map "e" 'find-file)
(define-key evil-ex-map "W" 'save-buffer)

;;; Vertico -- Vertical Interactive Completions
;;; and support packages that make vertico even better
;;; or use vertico optimally like consult
(use-package vertico
  :ensure t
  :bind (:map vertico-map
	      ("C-j" . vertico-next)
	      ("C-k" . vertico-previous)
	      ("C-f" . vertico-exit)
	      :map minibuffer-local-map
	      ("M-h" . backward-kill-word))
  :init
  (vertico-mode 1))

(use-package orderless
  :ensure t
  :custom
  (completion-styles '(orderless basic))
  (completion-category-overrides '((file (styles basic partial-completion)))))

(use-package savehist
  :init
  (savehist-mode))

(use-package marginalia
  :after vertico
  :ensure t
  :custom
  (marginalia-annotators '(marginalia-annotators-heavy marginalia-annotators-light nil))
  :init
  (marginalia-mode))

(use-package consult
  :ensure t
  :bind
  ("C-x b" . consult-buffer)
  ("C-x L" . consult-line))


;;; Org-mode
(use-package org
  :init
  (setq org-todo-keywords
       '((sequence "TODO" "WAIT" "SOMEDAY" "PROJ" "|" "DONE" "DELEGATED")))
  (setq org-ellipsis "  "
	org-hide-emphasis-markers t)) 

(use-package org-roam
  :ensure t
  :custom
  (org-roam-directory "~/documents/brain")
  :bind (("C-c n l" . org-roam-buffer-toggle)
	 ("C-c n f" . org-roam-node-find)
	 ("C-c n i" . org-roam-node-insert))
  :config
  (org-roam-setup))

(use-package evil-org
  :ensure t
  :after (evil org)
  :config
  (add-hook 'org-mode-hook 'evil-org-mode)
  (add-hook 'evil-org-mode-hook
            (lambda ()
              (evil-org-set-key-theme '(navigation insert textobjects additional calendar))))
  (require 'evil-org-agenda)
  (evil-org-agenda-set-keys))

;;; add rust-mode
(use-package rust-mode
  :ensure t
  :bind (("C-c C-c" . rust-run))
  :config
  (add-hook 'rust-mode-hook
            (lambda () (setq indent-tabs-mode nil))
	    (lambda () (prettify-symbols-mode)))
  (setq rust-format-on-save t)
  (add-hook 'rust-mode-hook 'eglot-ensure)
