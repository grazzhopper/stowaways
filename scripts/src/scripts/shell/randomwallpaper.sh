#!/usr/bin/env bash
# depends on feh
while true; do
  feh --randomize --bg-fill $1/*
  sleep $2
done
