#!/usr/bin/env bash

while true; do
    CURRENTDATE="$(date +'%d %b, %Y %H:%M')"
    BATTPERCENTAGE="$(upower -i /org/freedesktop/UPower/devices/DisplayDevice | awk '$1 == "percentage:" {print $2}')"
    xsetroot -name "bat: ${BATTPERCENTAGE} | ${CURRENTDATE}"
    sleep 1m
done &
