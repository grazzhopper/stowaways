# ~/.bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# aliasses
alias ls='exa'
alias la='ls -a'
alias ll='ls -l'
alias lla='ls -al'
alias hx='helix'
alias stow='stow --dotfiles'
alias startrader='startrader.py'

# adding stuff to PATH
export PATH="$HOME/.local/bin:$PATH"
export PATH="$HOME/tools/node-v16.15.0-linux-x64/bin:$PATH"
export PATH="$HOME/dev/startrader:$PATH"

# adding pyenv
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init --path)"
  eval "$(pyenv virtualenv-init -)"
fi

# adding doom-emacs to path
export PATH="$HOME/.emacs.d/bin:$PATH"
export PATH="$HOME/.poetry/bin:$PATH"

# eye candy
ufetch
eval "$(starship init bash)"

. "$HOME/.cargo/env"
