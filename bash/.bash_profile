# acticate if you want to use .bashrc in the login shell
# [[ -f ~/.bashrc ]] && . ~/.bashrc

# if loginshell then startx
if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
  # export _JAVA_AWT_WM_NONREPARENTING=1
  exec startx
fi

# adding pyenv to shell
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi
. "$HOME/.cargo/env"

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
    . "$HOME/.bashrc"
    fi
fi

export PATH="$HOME/.poetry/bin:$PATH"
