require"zen-mode".setup {
  window = {
    width = 90,
    options = {
      number = true,
      list = true,
      relativenumber = true
    }
  },
  plugins = {
    tmux = { enabled = true }
  }
}
