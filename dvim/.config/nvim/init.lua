-- setting the options
local set = vim.o
local setb = vim.bo
local setg = vim.g

set.expandtab = true
set.tabstop = 4
set.shiftwidth = 4
set.backup = false
set.clipboard = "unnamedplus"
set.fileencoding = "utf-8"
set.hlsearch = true
set.ignorecase = true
set.mouse = "a"
set.pumheight = 10
set.showtabline = 0
set.smartcase = true
set.splitbelow = true
set.splitright = true
set.swapfile = false
set.undofile = true
set.writebackup = false
set.cursorline = true
set.number = true
set.relativenumber = true
set.numberwidth = 3
set.signcolumn = "yes"
set.wrap = true
set.scrolloff = 8
set.completeopt = "menuone,noselect"
set.termguicolors = true
set.timeoutlen = 0

setb.autoindent = true
setb.smartindent = true

-- packages
require("packer").startup(function(use)
    use "wbthomason/packer.nvim"
    use "Soares/base16.nvim"
    use "nvim-lua/popup.nvim"
    use "windwp/nvim-autopairs"
    use {
        "nvim-treesitter/nvim-treesitter",
        run = ":TSUpdate"
    }
    use "akinsho/toggleterm.nvim"

    use "nvim/nvim-lspconfig"

    use { 'hrsh7th/nvim-cmp' }
    use { 'hrsh7th/cmp-buffer' }
    use { 'hrsh7th/cmp-path' }
    use { 'saadparwaiz1/cmp_luasnip' }
    use { 'hrsh7th/cmp-nvim-lsp' }
    use { 'hrsh7th/cmp-nvim-lua' }
    -- Snippets
    use { 'L3MON4D3/LuaSnip' }
    use { 'rafamadriz/friendly-snippets' }
    use {
        'nvim-telescope/telescope.nvim', tag = '0.1.0',
        requires = { { 'nvim-lua/plenary.nvim' } }
    }
    -- use { "folke/which-key.nvim", }
end)


-- keymaps
setg.mapleader = ","
local function map(mode, lhs, rhs, opts)
    local options = { noremap = true }
    if opts then options = vim.tbl_extend('force', options, opts) end
    vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

-- set colorscheme and transparent background
setg.base16_transparent_background = false
vim.cmd "colorscheme flat"


-- configure treesitter
require 'nvim-treesitter.configs'.setup {
    ensure_installed = { "c", "lua", "rust", "python", "bash", "html", "javascript", "json", "markdown", "rst", "sql",
        "yaml", "norg" },
    sync_install = false,
    auto_install = true,
    highlight = {
        enable = true,
        additional_vim_regex_highlighting = false,
    },
}


-- configure toggleterm
require("toggleterm").setup {
    size = 20,
    open_mapping = [[<c-\>]],
    hide_numbers = true,
    shade_terminals = true,
    shading_factor = 2,
    start_in_insert = true,
    insert_mappings = true,
    persist_size = true,
    direction = "float",
    close_on_exit = true,
    shell = vim.o.shell,
    float_opts = {
        border = "curved",
    },
}


-- lsp
require('cmp').setup{}
require('lspconfig')['pylsp'].setup {
    on_attach = function()
        vim.g.mapleader = " "
        vim.keymap.set("n", "K", vim.lsp.buf.hover, { buffer = 0, noremap = true, silent = true })
        vim.keymap.set("n", "gd", vim.lsp.buf.definition, { buffer = 0, noremap = true, silent = true })
        vim.keymap.set("n", "<leader>dj", vim.diagnostic.goto_next, { buffer = 0, noremap = true, silent = true })
        vim.keymap.set("n", "<leader>dk", vim.diagnostic.goto_prev, { buffer = 0, noremap = true, silent = true })
        vim.keymap.set("n", "<leader>dl", "<cmd>Telescope diagnostics<cr>", { buffer = 0, noremap = true, silent = true })
    end,
}
require('lspconfig').sumneko_lua.setup {}
require('lspconfig')['rust_analyzer'].setup {}
